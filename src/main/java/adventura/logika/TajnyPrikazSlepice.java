package adventura.logika;


/**
 *  Třída TajnyPrikazSlepice implementuje pro hru příkaz slepice.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */

public class TajnyPrikazSlepice implements IPrikaz {
    public static final String NAZEV = "slepice";
    private HerniPlan plan;
    private Hra hra;
    public TajnyPrikazSlepice(HerniPlan plan,Hra hra) {
        this.plan = plan;
        this.hra = hra;
    }

    /**
     * tajný příkaz, který potřebuje hráč k vyhrání hry.
     * @param parametry nazev věci
     * @return zpráva,  která se vypíše hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevim co mam prdnout";
        }
        else {
            if(plan.getAktualniProstor().getNazev().equals("tesco")) {
                if(parametry[0].equals("krabicak")) {
                    hra.setKonecHry(true);
                    return "aaaaahh konecne vytouzeny krabicak";
                }
                else {
                    return "to nechci potrebuju krabicak";
                }
            }
            else {
                return "tady nemuzu";
            }
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
