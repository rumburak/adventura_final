package adventura.logika;

/**
 *  Třída PrikazOdeber implementuje pro hru příkaz odeber.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */

public class PrikazOdeber implements IPrikaz {
    public static final String NAZEV = "odeber";
    HerniPlan plan;
    Hrac hrac;
    public PrikazOdeber(HerniPlan plan, Hrac hrac) {
        this.plan = plan;
        this.hrac = hrac;
    }

    /**
     * Pokusí se odebrat věc z batohu. Pokud se věch v batohu nenavhází, tak vypíše chybovou hlášku
     * @param parametry jako parametr dostane věc, kterou chce hráč odberat z batohu
     * @return zpráv, která se vypíše hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "Nevím, jakou věc mám odebrat";
        }
        else{
            String nazev = parametry[0];
            if(hrac.getBatoh().jeVBatohu(nazev)) {
                hrac.getBatoh().odstranZBatohu(nazev);
                Vec vracenaVec = new Vec(nazev, true);
                plan.getAktualniProstor().pridejVec(vracenaVec);
                return "tato věc byla odstraněna: " + nazev;
            }
            else {
                return "Věc se nenachází v batohu";
            }
        }
    }

    @Override
    public String getNazev()
    {
        return NAZEV;
    }
}
