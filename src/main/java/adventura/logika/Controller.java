package adventura.logika;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    public static final int SIRKA_IKONY = 45;
    public static final int VYSKA_IKONY = 30;

    @FXML
    private VBox seznamVychodu;
    @FXML
    private VBox seznamPredmetuVMistnosti;
    @FXML
    private VBox seznamPredmetuVBatohu;
    private Hra hra;

    public ImageView obrazekLokace;
    @FXML
    private Label popisLokace;
    @FXML
    private Label jmenoLokace;

    @FXML
    private VBox jdiZpet;

    @FXML
    private VBox seznamPostav;
    @FXML
    private Label akcniDialog;

    @FXML
    private StackPane mainPane;
    private Vec vybranaVec;
    private  boolean znaSlepici;
    private  boolean znaHesloMafie;
    private Prostor initProstor;
    private boolean plnyBatoh;

    public void setHra(Hra hra) {
        znaSlepici = false;
        znaHesloMafie = false;
        this.hra = hra;
        plnyBatoh = false;
        this.vybranaVec = null;
        initProstor = hra.getHerniPlan().getAktualniProstor();
        HerniPlan herniPlan = hra.getHerniPlan();
        Prostor aktualniProstor = herniPlan.getAktualniProstor();
//        jdiZpet.setOnMouseClicked(event -> {
//            hra.zpracujPrikaz("jdi zpet");
//            zmenProstor(hra.getHerniPlan().getAktualniProstor());
//        });
        zmenProstor(aktualniProstor);
        akcniDialog.setText(hra.vratUvitani());
    }

    public void jdiZpet() {
        hra.zpracujPrikaz("jdi zpet");
        zmenProstor(hra.getHerniPlan().getAktualniProstor());
    }

    private void zmenProstor(Prostor prostor) {
        akcniDialog.setText("");
        hra.zpracujPrikaz("jdi " + prostor.getNazev());
        System.out.println(hra.zpracujPrikaz("vypis batoh"));

        jmenoLokace.setText(prostor.getNazev());
        popisLokace.setText(prostor.getPopis());

        String nazevObrazku = "/img/" + prostor.getNazev() +  ".jpg";
        Image image = new Image(getClass().getResourceAsStream(nazevObrazku));
        obrazekLokace.setImage(image);
        obrazekLokace.setFitWidth(Screen.getPrimary().getVisualBounds().getWidth());
        obrazekLokace.setFitHeight(Screen.getPrimary().getVisualBounds().getHeight());

        pridejVychody(prostor);
        pridejPredmety(prostor);
        pridejPostavy(prostor);
    }

    private void pridejVychody(Prostor prostor) {
        seznamVychodu.getChildren().clear();
        for (Prostor p : prostor.getVychody()) {
            HBox vychod = new HBox();
            vychod.setSpacing(10);
            vychod.getStyleClass().add("kontejner");
            Label nazevProstoru = new Label(p.getNazev());

            ImageView vychodImageView = new ImageView();
            String nazevObrazku = "/img/" + p.getNazev() + ".jpg";
            Image vychodImage = new Image(getClass().getResourceAsStream(nazevObrazku));
            obrazekLokace.setImage(vychodImage);
            vychodImageView.setFitHeight(VYSKA_IKONY);
            vychodImageView.setFitWidth(SIRKA_IKONY);
            vychodImageView.setImage(vychodImage);


            vychod.getChildren().addAll(vychodImageView, nazevProstoru);

            seznamVychodu.getChildren().add(vychod);
            vychod.setOnMouseClicked(event -> {
                zmenProstor(p);
            });
        }
    }

    private void pridejPredmety(Prostor prostor) {
        seznamPredmetuVMistnosti.getChildren().clear();

        for (Vec vec : prostor.getSeznamVeci()) {
            pridejPredmetDoMistnosti(vec);
        }
    }

    private void pridejPredmetDoMistnosti(Vec vec) {
        HBox predmet  = new HBox();
        predmet.setSpacing(10);
        predmet.getStyleClass().add("kontejner");
        ImageView predmetImageView = new ImageView();
        String nazevObrazku = "/img/" + vec.getNazev() + ".jpg";
        Image predmetImage = new Image(getClass().getResourceAsStream(nazevObrazku));
        predmetImageView.setFitHeight(VYSKA_IKONY);
        predmetImageView.setFitWidth(SIRKA_IKONY);
        predmetImageView.setImage(predmetImage);
        Label nazevVeci = new Label(vec.getNazev());
        predmet.getChildren().addAll(predmetImageView, nazevVeci);
        seznamPredmetuVMistnosti.getChildren().add(predmet);
        predmet.setOnMouseClicked(event -> {
            akcniDialog.setText(hra.zpracujPrikaz("seber " + vec.getNazev()));
            if (vec.isPrenositelna()) {
                if(!plnyBatoh) {
                    System.out.println("veci v batphu: " + hra.getHrac().getBatoh().vypisSeznamVeci() );
                    // akcniDialog.setText(hra.zpracujPrikaz("seber " + vec.getNazev()));
                    HBox predmetVBatohu = new HBox();
                    predmetVBatohu.setSpacing(10);
                    predmetVBatohu.setId(vec.getNazev());
                    predmetVBatohu.getStyleClass().add("kontejner");
                    ImageView predmetBatohImageView = new ImageView();
                    String nazev = "/img/" + vec.getNazev() + ".jpg";
                    Image vecBatohImage = new Image(getClass().getResourceAsStream(nazev));
                    predmetBatohImageView.setFitHeight(VYSKA_IKONY);
                    predmetBatohImageView.setFitWidth(SIRKA_IKONY);
                    predmetBatohImageView.setImage(vecBatohImage);
                    Label vecVBatohu = new Label(vec.getNazev());
                    predmetVBatohu.getChildren().addAll(predmetBatohImageView, vecVBatohu);
                    seznamPredmetuVBatohu.getChildren().add(predmetVBatohu);
                    seznamPredmetuVMistnosti.getChildren().remove(predmet);

                    predmetVBatohu.setOnMouseClicked(event1 -> {
                        if(event1.getClickCount() == 2) {
                            hra.zpracujPrikaz("odeber " + vec.getNazev());
                            seznamPredmetuVBatohu.getChildren().remove(predmetVBatohu);
                            pridejPredmetDoMistnosti(vec);
                            plnyBatoh = false;
                        }
                        if(event1.getClickCount() == 1) {
                            seznamPredmetuVBatohu.getChildren().forEach(el -> {
                                el.getStyleClass().removeAll("active");
                            });
                            predmetVBatohu.getStyleClass().add("active");
                            vybranaVec = vec;
                        }
                    });
                   plnyBatoh = hra.getHrac().getBatoh().jePlny();
                }
            } else if(vec.getNazev().equals("krabicak")) {
                checkKrabicak();
            } else if(vec.getVnitrniVec() != null) {
                //akcniDialog.setText(hra.zpracujPrikaz("prohledat " + vec.getNazev()));
                String prikaz = hra.zpracujPrikaz("prohledat " + vec.getNazev());
                zmenProstor(hra.getHerniPlan().getAktualniProstor());
                akcniDialog.setText(prikaz);
            }
        });
    }

    private void checkKrabicak() {
        if(!znaSlepici) {
            akcniDialog.setText("Nemuzes to sebrat obchod je strezen");
        } else {
            exitGame();
        }
    }

    private void pridejPostavu(HerniPostava postava) {
        HBox postavaBox  = new HBox();
        postavaBox.setSpacing(10);
        postavaBox.getStyleClass().add("kontejner");
        ImageView postavaImageView = new ImageView();
        String nazevObrazku = "/img/" + postava.getJmeno() + ".jpg";
        Image predmetImage = new Image(getClass().getResourceAsStream(nazevObrazku));
        postavaImageView.setFitHeight(VYSKA_IKONY);
        postavaImageView.setFitWidth(SIRKA_IKONY);
        postavaImageView.setImage(predmetImage);
        Label nazevPostavy = new Label(postava.getJmeno());
        postavaBox.getChildren().addAll(postavaImageView, nazevPostavy);
        seznamPostav.getChildren().add(postavaBox);
        postavaBox.setOnMouseClicked(event -> {
            if(vybranaVec != null) {
                akcniDialog.setText(hra.zpracujPrikaz("dej " + vybranaVec.getNazev() + " " + postava.getJmeno()));
                if(postava.getVecNaVymenu() != null && vybranaVec.getNazev().equals(postava.getVecNaVymenu())) {
                    Optional<Node> obj = seznamPredmetuVBatohu.getChildren().stream().filter(el -> el.getId()
                            .equals(vybranaVec.getNazev())).findFirst();
                    seznamPredmetuVBatohu.getChildren().removeAll(obj.get());
                    plnyBatoh = false;
                    if(postava.getJmeno().equals("maugli")) {
                        znaSlepici = true;
                    }
                    if(postava.getJmeno().equals("likvidator")) {
                        znaHesloMafie = true;
                        System.out.println("zna heslo mafie");
                    }

                }
                vybranaVec = null;
                seznamPredmetuVBatohu.getChildren().forEach(el -> {
                    el.getStyleClass().removeAll("active");
                });
            } else {
                if(postava.getJmeno().equals("mafian")) {
                    akcniDialog.setText(znaHesloMafie? hra.zpracujPrikaz("smrtijed " + "mafian") :
                            hra.zpracujPrikaz("mluv " + "mafian"));
                } else {
                    akcniDialog.setText(hra.zpracujPrikaz("mluv " + postava.getJmeno()));
                }
            }
        });


    }

    private void pridejPostavy(Prostor prostor) {
        seznamPostav.getChildren().clear();
        for(HerniPostava postava : prostor.getPostavy()) {
            pridejPostavu(postava);
        }
    }

    public void showDialog() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainPane.getScene().getWindow());
        dialog.setTitle("Napoveda");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/napoveda.fxml"));
        try{
            WebView browser = new WebView();
            WebEngine engine = browser.getEngine();
            engine.load(String.valueOf(getClass().getResource("/napoveda.html")));
            dialog.getDialogPane().setContent(browser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        Optional<ButtonType> result = dialog.showAndWait();
    }

    @FXML
    public void showMap() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainPane.getScene().getWindow());
        dialog.setTitle("Mapa");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/mapa.fxml"));
        try{
            dialog.getDialogPane().setContent(fxmlLoader.load());
            String nazevObrazku = "/img/mapa" + hra.getHerniPlan().getAktualniProstor().getNazev() + ".png";
            System.out.println(nazevObrazku);
            Image mapaImage = new Image(getClass().getResourceAsStream(nazevObrazku));
            ImageView view = new ImageView();
            view.setImage(mapaImage);
            view.setFitWidth(500);
            view.setFitHeight(500);
            Label label = new Label();
            dialog.getDialogPane().setContent(view);
        } catch (IOException e) {
            e.printStackTrace();
        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        Optional<ButtonType> result = dialog.showAndWait();
    }
    @FXML
    private void endGame() {
        try
        {
            jmenoLokace.setText("EL FIN");
            popisLokace.setText("VIEL DANK");
            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            // log the exception.
        }
        finally {
            System.exit(0);
        }

    }

    public void exitGame() {
        if(znaSlepici) {
            jmenoLokace.setText(hra.vratEpilog());
            popisLokace.setText("Dekuji, ze jste si zahrali!");
            TimedExit exit = new TimedExit();
        } else {
            jmenoLokace.setText("KONEC");
            popisLokace.setText("VIEL DANK");
            TimedExit exitApp = new TimedExit();

        }
    }

    public class TimedExit {
        Timer timer = new Timer();
        TimerTask exitApp = new TimerTask() {
            public void run() {
                System.exit(0);
            }
        };
        public TimedExit() {
            timer.schedule(exitApp, new Date(System.currentTimeMillis()+2000));
        }

    }


    @FXML
    private void newGame() {
        hra.getHerniPlan().setAktualniProstor(initProstor);
       seznamPredmetuVBatohu.getChildren().forEach(el -> hra.zpracujPrikaz("odeber " + el.getId()));
        seznamPredmetuVBatohu.getChildren().clear();
        vybranaVec = null;
        znaHesloMafie = false;
        znaSlepici = false;
        zmenProstor(initProstor);
        setHra(new Hra());
    }
}
