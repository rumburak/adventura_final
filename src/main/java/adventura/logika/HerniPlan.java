package adventura.logika;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author  Michal Vokolek
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor hlavniNadrazi = new Prostor("HlavniNadrazi","Starý dobrý Hlavák, tvůj rajón");
        Prostor temnaZakouti = new Prostor("TemnaZakouti", "V těchto místech je to velmi nebezpečné, raději s nikým nemluv");
        Prostor popelnice = new Prostor("popelnice","V v této popelnici občas najdeš opravdu kuriózní věci");
        Prostor tesco = new Prostor("tesco","tento obchod je 24 hodin monitorovaný \n muzes si tu koupit krabicák za 30kč");
        Prostor tramvaj = new Prostor("tramvaj"," v této tramvaji trávíš deštivé dny.");
        Prostor tajneMisto = new Prostor("tajneMisto", "místo, kde se schází ");
        Prostor squat = new Prostor("squat","V tomhle squatu sis dal poprvé a taky naposled matečák");
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        hlavniNadrazi.setVychod(temnaZakouti);
        hlavniNadrazi.setVychod(popelnice);
        hlavniNadrazi.setVychod(tramvaj);
        popelnice.setVychod(tesco);
        tramvaj.setVychod(tajneMisto);
        tajneMisto.setVychod(squat);
        squat.setVychod(temnaZakouti);

        aktualniProstor = hlavniNadrazi;

        // prirazeni veci do prostoru
        hlavniNadrazi.pridejVec(new Vec("kos", false));
        hlavniNadrazi.pridejVec(new Vec("rozpadlyKufr", false, "rum"));
        hlavniNadrazi.pridejVec(new Vec("vajgl", true));
        temnaZakouti.pridejVec(new Vec("flaska", true));
        popelnice.pridejVec(new Vec("popelnice", false));
        tramvaj.pridejVec(new Vec("noviny", true));
        tesco.pridejVec(new Vec("krabicak", false));
        tajneMisto.pridejVec(new Vec("ker", false, "matecak"));
        squat.pridejVec(new Vec("spacak", false));
        squat.pridejVec(new Vec("mrtvola", false, "nuz"));

        //prirazeni postav do prostoru
        hlavniNadrazi.pridejPostavu(new HerniPostava("rumburak", "Cóžé, já že jsem Rumburak?"));
        hlavniNadrazi.pridejPostavu(new HerniPostava("maugli", "Takhle se tady zfoukáváme! \n když mi přineseš matečák tak" +
                "ti řeknu, jak sehnat na 100% krabicák! je to taková flaška", "matecak", "BRRRRAAAAA! diky konecne se muzu zfoukat" +
                "Hele řeknu ti jak dostaneš krabicu, jdi do tesca a schovej se prostš v nejaký slepici, nidko tě neuvidi 100% garance"));
        tramvaj.pridejPostavu(new HerniPostava("staryPan", "Ty kanále jeden zmizni!"));
        tajneMisto.pridejPostavu(new HerniPostava("mafian", "s tebou se bavit nebudu", "mauglihoHlava", "" +
                "konečně je ta svině mrtvá. tady máš odměnu 30 táců."));
        temnaZakouti.pridejPostavu(new HerniPostava("likvidator", "Nazdar kámo, nemaš nějakou bozenu? řeknu ti tajný kód mafiánů když mi nějakou přineseš", "rum"
        , "PaPaParádo dneska bude kantáre, ten kód jsem zaslech zněl myslím takhle: SMRTIJED"));
        squat.pridejPostavu(new HerniPostava("stranskej", " už sem tam měl bejt 4 minuty, já sem m*g*r!"));

    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }

}
