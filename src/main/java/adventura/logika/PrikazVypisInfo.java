package adventura.logika;


/**
 *  Třída PrikazVypisInfo implementuje pro hru příkaz vypis.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */

public class PrikazVypisInfo implements IPrikaz {
    private static final String NAZEV = "vypis";
    Hrac hrac;
    public PrikazVypisInfo(Hrac hrac) {
        this.hrac = hrac;
    }

    /**
     * vypíše vlastnosti, nebo obsah batohu hráče
     * @param parametry jako parametr dostane buď batoh nebo vlastnosti
     * @return zpráva, která se vypíše hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevím jaké informace myslíš";
        }
        else {
            String informace = parametry[0];
            if(informace.equals("batoh")) {
                return hrac.getBatoh().vypisSeznamVeci();
            } else if(informace.equals("vlastnosti")) {
                return hrac.vypisVlastnosti();
            } else {
                return "nevim co myslis";
            }
        }

    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
