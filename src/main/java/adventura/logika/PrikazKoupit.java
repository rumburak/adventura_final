package adventura.logika;

public class PrikazKoupit implements IPrikaz {
    public static final String NAZEV = "koupit";
    private Hrac hrac;
    private HerniPlan plan;
    private Hra hra;

    public PrikazKoupit(HerniPlan plan, Hrac hrac, Hra hra) {
        this.plan = plan;
        this.hrac = hrac;
        this.hra = hra;
    }
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "A co si mam koupit?";
        }
        else {
            String vec = parametry[0];
            if(plan.getAktualniProstor().vecExistuje(vec)) {
                if(vec.equals("krabicak")) {
                    if(hrac.getPenize() >= 30) {
                        hra.setKonecHry(true);
                        return "Konečně si můžu koupit krabicák";
                    }
                    else {
                        return  "Doprdele nemám na to peníze";
                    }
                }
                else {
                    return "Proč bych si to kupoval?";
                }
            }
            else {
                return "tuhle věc si ani nemůžu koupit";
            }
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
