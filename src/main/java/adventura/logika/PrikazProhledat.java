package adventura.logika;


/**
 *  Třída PrikazProhledat implementuje pro hru příkaz prohledat.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */

public class PrikazProhledat implements IPrikaz {
    private HerniPlan plan;
    public static final String NAZEV = "prohledat";

    public PrikazProhledat(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *
     * @param parametry jako parametr dostane název věcí, kterou chce hráč prohledat
     * @return zpráva, která se vypíše hráčí
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "Nevím, jakou vec myslis";
        }
        else {
            String nazev = parametry[0];
            if(plan.getAktualniProstor().ziskejVec(nazev).getVnitrniVec() == null) {
                return "Věc nejde prohledat";
            }
            else {
                if(!plan.getAktualniProstor().ziskejVec(nazev).bylaProhledana()) {
                    String x = plan.getAktualniProstor().ziskejVec(nazev).ziskejVnitrniVec();
                    Vec novaVec = new Vec(x, true);
                    plan.getAktualniProstor().pridejVec(novaVec);
                    Prostor refresh = plan.getAktualniProstor();
                    plan.setAktualniProstor(refresh);
                    return "našel si tuto věc: " + plan.getAktualniProstor().ziskejVec(x).getNazev();
                } else return "Věc je už prázndá";
            }
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
