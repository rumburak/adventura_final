package adventura.logika;

import java.util.Stack;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Michal Vokolek
 */
public class PrikazJdi implements IPrikaz {
    Stack<Prostor> trasa;
    private Hrac hrac;
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazJdi(HerniPlan plan, Hrac hrac)
    {
        this.plan = plan;
        this.hrac = hrac;
        trasa = new Stack<>();
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@return zpráva, kterou vypíše hra hráči
     * @param parametry - jako  parametr obsahuje jméno prostoru (východu),
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];
        if(smer.equals("zpet")) {
            if(!trasa.isEmpty()) {
                plan.setAktualniProstor(trasa.peek());
                hrac.setZivot(hrac.getZivot() - 1);
                return trasa.pop().dlouhyPopis();
            }
            else return "už jsem na začátku nemohu jít zpět";
        }

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);

        if (sousedniProstor == null) {
            return "Tam se odsud jít nedá!";
        }
        else {
            trasa.push(plan.getAktualniProstor());
            plan.setAktualniProstor(sousedniProstor);
            hrac.setZivot(hrac.getZivot()-1);
            return sousedniProstor.dlouhyPopis();
        }
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
