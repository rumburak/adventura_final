package adventura.logika;

/**
 *  Třída PrikazSeber implementuje pro hru příkaz seber.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */

public class PrikazSeber implements IPrikaz {

    private final static String NAZEV = "seber";
    private final HerniPlan herniPlan;
    private Hrac hrac;

    public PrikazSeber(HerniPlan plan, Hrac hrac) {
        herniPlan = plan;
        this.hrac = hrac;

    }

    /**
     * pokusí se sebrat věc z prostoru. Pokud má hráč plný batoh, nebo tam věc není, vypíše chybu
     * @param parametry jako parametr dostane název věci kterou chce hráč sebrat
     * @return zpráva,  kerá se vypíše hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevím co mám sebrat";
        }

        String nazevVeci = parametry[0];
        Prostor prostor = herniPlan.getAktualniProstor();
        if(!prostor.vecExistuje(nazevVeci)) {
            return "věc neexistuje";
        }
        Vec vec = prostor.ziskejVec(nazevVeci);
        if(vec.isPrenositelna()) {
            if(hrac.getBatoh().vlozDoBatohu(vec)) {
                prostor.odstranVec(nazevVeci);
                return "věc přidána";
            } else {
                return "máš plný batoh";
            }
        }
        return "tuto věc nelze přenést";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }

}
