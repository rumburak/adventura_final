package adventura.logika;


/**
 *  Třída HerniPostava představuje postavu, se kterou se hráč setká ve hře.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author    Michal Vokolek
 */
public class HerniPostava {
    private String jmeno;
    private String monolog;
    private String vecNaVymenu;
    private String monologPoVymene;

    /**
     *
     *
     * @param jmeno jméno postavy
     * @param monolog text, který postava říká při setkání
     */
    public HerniPostava(String jmeno, String monolog) {
        this(jmeno, monolog, null, null);
    }

    /**
     * konstruktor pro postavy, které chtějí nějakou věc
     * @param jmeno jméno postavy
     * @param monolog  text, který postava říká při setkání
     * @param vec věc kterou postava chce
     * @param monolog2 text, který říká při předání věci
     */
    public HerniPostava(String jmeno, String monolog, String vec, String monolog2) {
        this.jmeno = jmeno;
        this.monolog = monolog;
        vecNaVymenu = vec;
        monologPoVymene = monolog2;
    }
    public String getJmeno() {
        return jmeno;
    }

    public String mluv() {
        return monolog;
    }
    public String getVecNaVymenu() {
        return vecNaVymenu;
    }
    public String getMonologPoVymene() {

        return monologPoVymene;
    }
}
