package adventura.logika;

/**
 *  Třída PrikazZabij implementuje pro hru příkaz zabit.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */
public class PrikazZabij implements IPrikaz {
    private Hrac hrac;
    private HerniPlan plan;
    public static final String NAZEV = "zabit";
    public PrikazZabij(HerniPlan plan, Hrac hrac) {
        this.plan = plan;
        this.hrac = hrac;
    }

    /**
     * pokusí se zabít jen jednu konkrétní postavu podle úkolu. jiné postavy nejdou zabít
     * @param parametry jako parametr dotane jmeno postavi
     * @return zpráva, která se vypíše hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevim koho mam zabit";
        }
        else {
            String jmeno = parametry[0];
            if(plan.getAktualniProstor().postavaExistuje(jmeno) && jmeno.equals("maugli")) {
                if(hrac.getBatoh().jeVBatohu("nuz")) {
                    plan.getAktualniProstor().odstranPostavu("maugli");
                    plan.getAktualniProstor().pridejVec(new Vec("mauglihoHlava", true));
                    return "zabil si mauglího matečáka";
                }
                else {
                    return "nemám ho jak zabít";
                }
            }
            else if(plan.getAktualniProstor().postavaExistuje(jmeno)) {
                return "tuhle postavu nebudu zabíjet";
            }
            else {
                return "tato postava tady není";
            }
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
