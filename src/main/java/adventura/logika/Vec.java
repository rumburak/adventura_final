package adventura.logika;

/**
 *  Třída Vec představuje věc, která se nachází v prostoru hry
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */
public class Vec {
    private final String nazev;
    private final boolean prenositelna;
    private String vnitrniVec;
    private boolean vecBylaProhledana;

    /**
     * Konstruktor pro věc, která se nedá prohledat
     * @param nazev název věci
     * @param prenositelna boolean hodnota, zda se da věc uloži do batohu
     */
    public  Vec(String nazev, boolean prenositelna) {
        this.nazev = nazev;
        this.prenositelna = prenositelna;
        vnitrniVec = null;
        vecBylaProhledana = false;
    }

    /**
     * Konstruktor pro věc, ve které se ještě něco nachází
     * @param nazev
     * @param prenositelna
     * @param vnitrniVec název věci, která je ukryta uvnitř dané věci
     */
    public  Vec(String nazev, boolean prenositelna, String vnitrniVec) {
        this.nazev = nazev;
        this.prenositelna = prenositelna;
        this.vnitrniVec = vnitrniVec;
    }

    public String getNazev() {
        return nazev;
    }
    public boolean bylaProhledana() {
        return vecBylaProhledana;
    }

    public boolean isPrenositelna() {
        return prenositelna;
    }
    public String getVnitrniVec() {
        return vnitrniVec;
    }
    public String ziskejVnitrniVec() {
        vecBylaProhledana = true;
        return vnitrniVec;
    }
}

