package adventura.logika;
/**
 *  Třída PrikazDej implementuje pro hru příkaz dej.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */

public class PrikazDej implements IPrikaz {
    HerniPlan plan;
    Hrac hrac;
    /**
     *  Konstruktor třídy
     *
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     *  @param hrac postava, za kterou hráč hraje"
     */
    public PrikazDej(HerniPlan plan, Hrac hrac) {
        this.plan = plan;
        this.hrac = hrac;
    }
    public static final String NAZEV = "dej";

    /**
     * Zkouší dát věc postavě v prostoru. POkud postava neexistuje, nebo věc neexistuje, nebo postava věc nechce, tak vypíše chybové hlášení
     * @param parametry nazev veci a jmeno postavy
     * @return zpráva, kterou vypíše hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevím co mám dát";
        }
        else {
            String vec = parametry[0];
            if(parametry.length  <= 1 ) {
                return "nevím komu to mám dát";
            }
            String jmeno = parametry[1];
            if(!hrac.getBatoh().jeVBatohu(vec)) {
                return "tuhle věc nemám v batohu";
            }
            if(!plan.getAktualniProstor().postavaExistuje(jmeno)) {
                return "tahle postava tady neni";
            } else {
                if(plan.getAktualniProstor().getPostava(jmeno).getVecNaVymenu() == null
                        || !plan.getAktualniProstor().getPostava(jmeno).getVecNaVymenu().equals(vec)) {
                    return "tuhle věc postava nechce";
                } else {
                    hrac.getBatoh().odstranZBatohu(vec);
                    if(plan.getAktualniProstor().getPostava(jmeno).getJmeno().equals("mafian")) {
                        hrac.setPenize(30000);
                    }
                    return plan.getAktualniProstor().getPostava(jmeno).getMonologPoVymene();
                }
            }
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
