package adventura.logika;

/**
 *  Třída Hra - třída představující logiku adventury.
 * 
 *  Toto je hlavní třída  logiky aplikace.  Tato třída vytváří instanci třídy HerniPlan, která inicializuje mistnosti hry
 *  a vytváří seznam platných příkazů a instance tříd provádějící jednotlivé příkazy.
 *  Vypisuje uvítací a ukončovací text hry.
 *  Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 */

public class Hra implements IHra {
    private SeznamPrikazu platnePrikazy;    // obsahuje seznam přípustných příkazů
    private HerniPlan herniPlan;
    private Hrac hrac;
    private boolean konecHry = false;

    /**
     *  Vytváří hru a inicializuje místnosti (prostřednictvím třídy HerniPlan) a seznam platných příkazů.
     */
    public Hra() {
        herniPlan = new HerniPlan();
        hrac = new Hrac();
        platnePrikazy = new SeznamPrikazu();
        platnePrikazy.vlozPrikaz(new PrikazNapoveda(platnePrikazy));
        platnePrikazy.vlozPrikaz(new PrikazJdi(herniPlan, hrac));
        platnePrikazy.vlozPrikaz(new PrikazKonec(this));
        platnePrikazy.vlozPrikaz(new PrikazSeber(herniPlan, hrac));
        platnePrikazy.vlozPrikaz(new PrikazVypisInfo(hrac));
        platnePrikazy.vlozPrikaz(new PrikazMluv(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazProhledat(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazOdeber(herniPlan, hrac));
        platnePrikazy.vlozPrikaz(new PrikazDej(herniPlan, hrac));
        platnePrikazy.vlozPrikaz(new TajnyPrikazSlepice(herniPlan, this));
        platnePrikazy.vlozPrikaz(new TajnyPrikazSmrtijed(herniPlan, hrac));
        platnePrikazy.vlozPrikaz(new PrikazZabij(herniPlan, hrac));
        platnePrikazy.vlozPrikaz(new PrikazKoupit(herniPlan, hrac, this));
    }

    /**
     *  Vrátí úvodní zprávu pro hráče.
     */
    public String vratUvitani() {
        return "Vítej ve hře!\n" +
                "Právě ses probudil z bezesné noci. je To teprve pět minut co ses probudil, ale už cítíš nezvratné nutkání zavlažit své hrdlo\n" +
               " Bez zaváhání popadneš svůj ruksak a reflexivně pátráš po krabicáku... \n" +
               "................................................................................\n" +
                "TO NE, JE PRYČ! \n" +
                "v panice vysypeš obsah svého batohu a zjisťuješ, že tě někdo okradl" +
                "Bez krabicáku přežiješ maximálně 24 hodin. \n" +
                "Čas běží. Za každou cenu musíš získat aspoň doušek.\n" +
                herniPlan.getAktualniProstor().dlouhyPopis();
    }
    
    /**
     *  Vrátí závěrečnou zprávu pro hráče.
     */
    public String vratEpilog() {
        return "Konečně! z toho pekelného moratoria mě spasil můj krabicák. Vino tinto Není lepšího! \n Opět spatřím hvězdné nebe na mnou" +
                " s douškem krabicáku ve mně";
    }
    
    /** 
     * Vrací true, pokud hra skončila.
     */
     public boolean konecHry() {
        return konecHry;
    }

    /**
     *  Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo příkazu a další parametry.
     *  Pak otestuje zda příkaz je klíčovým slovem  např. jdi.
     *  Pokud ano spustí samotné provádění příkazu.
     *
     *@param  radek  text, který zadal uživatel jako příkaz do hry.
     *@return          vrací se řetězec, který se má vypsat na obrazovku
     */
     public String zpracujPrikaz(String radek) {
        String [] slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String []parametry = new String[slova.length-1];
        for(int i=0 ;i<parametry.length;i++){
           	parametry[i]= slova[i+1];  	
        }
        String textKVypsani=" .... ";
        if (platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = platnePrikazy.vratPrikaz(slovoPrikazu);
            textKVypsani = prikaz.provedPrikaz(parametry);
        }
        else {
            textKVypsani="Nevím co tím myslíš? Tento příkaz neznám. ";
        }
        return textKVypsani;
    }
    
    
     /**
     *  Nastaví, že je konec hry, metodu využívá třída PrikazKonec,
     *  mohou ji použít i další implementace rozhraní Prikaz.
     *  
     *  @param  konecHry  hodnota false= konec hry, true = hra pokračuje
     */
    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }
    
     /**
     *  Metoda vrátí odkaz na herní plán, je využita hlavně v testech,
     *  kde se jejím prostřednictvím získává aktualní místnost hry.
     *  
     *  @return     odkaz na herní plán
     */
     public HerniPlan getHerniPlan(){
        return herniPlan;
     }

    public Hrac getHrac() {
        return hrac;
    }
}

