package adventura.logika;


/**
 *  Třída TajnýPrikazSmrtjed implementuje pro hru příkaz smrtijed.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 */

public class TajnyPrikazSmrtijed implements IPrikaz {
    HerniPlan plan;
    Hrac hrac;
    public TajnyPrikazSmrtijed(HerniPlan plan, Hrac hrac) {
        this.plan = plan;
        this.hrac = hrac;
    }
    public static final String NAZEV = "smrtijed";

    /**
     * Tajný příkaz pro vyhrání hry. Umožní mluvit s mafiánem
     * @param parametry název postavy.
     * @return zpráva, která se vypíše hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "nevim, komu to mám říct.";
        }
        else {
            String nazev = parametry[0];
            if(plan.getAktualniProstor().postavaExistuje(nazev)) {
                if (nazev.equals("mafian")) {
                    return "Cože? ty znáš kód? Dobře, sejmi pro mě Mauglího ten šmejd mě okradl. Vyřeš to rychle a dát ti za to balikos";
                }
                else {
                        return "Kdo je smrtijed???";
                }
            }
            else {
                return "Postava tady neni";
            }
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
