package adventura.logika;
/**
 *  Třída PrikazMluv implementuje pro hru příkaz mluv.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michal Vokolek
 *@version    pro školní rok 2016/2017
 */

public class PrikazMluv implements IPrikaz {
    private static final String NAZEV = "mluv";
    private HerniPlan plan;
    public PrikazMluv(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Pokusí se mlvit s herní postavou. pokud postava není v porsotu, tak vypíše chybu
     * @param parametry jako parametr obsahuje jméno postavy
     * @return zpráva, která se vypíše hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "Nevím s kým mám mluvit";
        }
        else {
            String nazevPostavy = parametry[0];
            if(plan.getAktualniProstor().postavaExistuje(nazevPostavy)) {
                return plan.getAktualniProstor().getPostava(nazevPostavy).mluv();
            }
            else {
                return "Postava tu není";
            }
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
