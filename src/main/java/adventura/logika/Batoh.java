package adventura.logika;

import java.util.HashMap;
import java.util.Map;


/**
 *  Třída Batoh implementuje pro inventář hráče.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author   Michal Vokolek
 */
public class Batoh {
    // maximalni pocet veci ktere se vejdou do batohu
    public int maxVeci = 3;
    //sbirat vice veci se stejnym nazvem ve hre nejde
    private HashMap<String, Vec> seznamVeci;
    //pro testovaci ucely je mozne zadat velikost batohu
    public Batoh(int pocet) {
        seznamVeci = new HashMap<>();
        maxVeci = pocet;
    }
    public Batoh() {
        seznamVeci = new HashMap<>();
    }

    /**
     *
     * @param vec
     * @return boolean hodnota pro testovani
     * umoznuje vkladat vec do batohu, pokud neni plny
     */
    public boolean vlozDoBatohu(Vec vec) {
        if(seznamVeci.size() == maxVeci) {
            System.out.println("Do batohu se ti už nic nevejde");
            return false;
        }
        else {
            seznamVeci.put(vec.getNazev(), vec);
            return true;
        }
    }
    public Vec ziskejVecZBatohu(String nazev) {
        return seznamVeci.get(nazev);
    }


    /**
     *
     * @param nazev
     * @return boolean hodnota pro testy
     * umoznuje odstranit vec z batohu, pokud se v batohu nachazi
     */

    public boolean odstranZBatohu(String nazev) {
        if(seznamVeci.containsKey(nazev)) {
            seznamVeci.remove(nazev);
            return true;
        }
        else {
            System.out.println("Tuhle vec v batohu nemam");
            return false;
        }
    }

    /**
     *
     * @return String reprezentujici seznam veci v batohu
     */
    public String vypisSeznamVeci() {
        if(seznamVeci.size() == 0) return "batoh je prázdný";
        StringBuilder b = new StringBuilder();
        for (Object o : seznamVeci.entrySet()) {
            Map.Entry prvek = (Map.Entry) o;
            b.append(prvek.getKey()).append(" ");
        }
        return b.toString().substring(0, b.length() - 1);
    }

    public boolean jeVBatohu(String vec) {
        return seznamVeci.containsKey(vec);
    }
    public boolean jePlny() {
        // viz controller
        return seznamVeci.size() == maxVeci;
    }

}
