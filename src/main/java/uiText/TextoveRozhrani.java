package uiText;


import java.io.*;
import java.util.Scanner;

import adventura.logika.Hra;

/**
 *  Class TextoveRozhrani
 * 
 *  Toto je uživatelského rozhraní aplikace Adventura
 *  Tato třída vytváří instanci třídy Hra, která představuje logiku aplikace.
 *  Čte vstup zadaný uživatelem a předává tento řetězec logice a vypisuje odpověď logiky na konzoli.
 *  
 *  
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */

public class TextoveRozhrani {
    private Hra hra;

    /**
     *  Vytváří hru.
     * @param hra
     */
    public TextoveRozhrani(Hra hra) {
        this.hra = hra;
    }

    /**
     *  Hlavní metoda hry. Vypíše úvodní text a pak opakuje čtení a zpracování
     *  příkazu od hráče do konce hry (dokud metoda konecHry() z logiky nevrátí
     *  hodnotu true). Nakonec vypíše text epilogu.
     */
    public void hraj() {
        System.out.println(hra.vratUvitani());

        // základní cyklus programu - opakovaně se čtou příkazy a poté
        // se provádějí do konce hry.

        while (!hra.konecHry()) {
            String radek = prectiString();
            System.out.println(hra.zpracujPrikaz(radek));
            if(hra.getHrac().getZivot() <= 0) {
                System.out.println("Z vypětím všech sil jsi udělal poslední kroky a svalil ses na mrazivou zem. Sen o krabicáků pomalu vyprchává" +
                        "tak jako tvoje vědomí. V tvém posledním okamžiku je ti už vše jedno a necháváš se porazit věčným spánkem \n" +
                        "konec hry");
                break;
            }
        }
        if(hra.getHrac().getZivot() > 0) {
            System.out.println(hra.vratEpilog());
        }
    }

    /**
     *  Metoda přečte příkaz z příkazového řádku
     *
     *@return    Vrací přečtený příkaz jako instanci třídy String
     */
    private String prectiString() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("> ");
        return scanner.nextLine();
    }
    public void hrajZeSouboru(String jmenoSouboru) {
        try(BufferedReader cteni = new BufferedReader(new FileReader(jmenoSouboru));
            PrintWriter zapis = new PrintWriter(new FileWriter("vystpu.txt"))
        ) {
            System.out.println(hra.vratUvitani());
            zapis.println(hra.vratUvitani());
            String radek = cteni.readLine();
            while(radek != null && !hra.konecHry()) {
                System.out.println("> " + radek);
                zapis.println("--> " + radek);
                String vystup = hra.zpracujPrikaz(radek);
                System.out.println(vystup);
                zapis.println(vystup);
                radek = cteni.readLine();
            }

            if(hra.konecHry()) {
                System.out.println(hra.vratEpilog());
                zapis.println(hra.vratEpilog());
            }

        } catch (FileNotFoundException e) {
            System.out.println("soubor nenalezen");
            File file = new File(jmenoSouboru);
            System.out.println("hledano : " + file.getAbsolutePath());
        }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("FINNALY");
        }
    }

}
