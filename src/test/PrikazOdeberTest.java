import adventura.logika.Hra;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrikazOdeberTest {


    private Hra hra;

    @Before
    public void setUp() {
        hra = new Hra();
    }

    @Test
    public void odeberTest() {
        hra.zpracujPrikaz("seber vajgl");
        hra.zpracujPrikaz("jdi tramvaj");
        hra.zpracujPrikaz("odeber vajgl");
        Assert.assertTrue(hra.getHerniPlan().getAktualniProstor().vecExistuje("vajgl"));
        Assert.assertFalse(hra.getHrac().getBatoh().jeVBatohu("vajgl"));
    }
}
