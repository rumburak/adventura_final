package logika;
import adventura.logika.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author    Jarmila Pavlíčková
 * @version  pro školní rok 2016/2017
 */
public class HraTest {
    private Hra hra1;

    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */
    @Test
    public void testPrubehHry() {
        hra1.zpracujPrikaz("jdi tramvaj");
        assertEquals(false, hra1.konecHry());
        assertEquals("tramvaj", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("konec");
        assertEquals(true, hra1.konecHry());
    }

    /**
     * Testuje, zda hráč již někam šel a tím pádem se může vrátit zpátky
     * hráč se vrací vždy o jednu lokaci zpět
     * pokud nikam nešel, nebo se vrátil na začátek, tak zůstává na místě
     */
    @Test
    public void testJdiZpet() {
        assertEquals("uz jsem na zacatku nemohu jit zpet", hra1.zpracujPrikaz("jdi zpet"));
        Prostor zacatek = hra1.getHerniPlan().getAktualniProstor();
        hra1.zpracujPrikaz("jdi popelnice");
        Prostor popelnice = hra1.getHerniPlan().getAktualniProstor();
        hra1.zpracujPrikaz("jdi tesco");
        hra1.zpracujPrikaz("jdi zpet");
        assertEquals(hra1.getHerniPlan().getAktualniProstor(),popelnice);
        hra1.zpracujPrikaz("jdi zpet");
        assertEquals(hra1.getHerniPlan().getAktualniProstor(), zacatek);
    }


    @Test
    public void testHracUmrel() {
        for(int i = 0; i < 24; i++) {
            String smer1 = "jdi tramvaj";
            String smer2 = "jdi zpet";
            if(i%2 == 0) {
                hra1.zpracujPrikaz(smer1);
            }
            else hra1.zpracujPrikaz(smer2);
        }
        Assert.assertEquals(0, hra1.getHrac().getZivot());
    }

}
