package logika;
import adventura.logika.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrikazKoupitTest {

    private  Hra hra;

    @Before
    public void setUp() {
        hra = new Hra();
    }

    @Test
    public void koupitTest() {
        hra.zpracujPrikaz("jdi popelnice");
        hra.zpracujPrikaz("jdi tesco");
       String res =  hra.zpracujPrikaz("koupit krabicak");
        Assert.assertEquals(res, "Doprdele nemám na to peníze");
        String res2 = hra.zpracujPrikaz("koupit papundekl");
        Assert.assertEquals(res2, "tuhle věc si ani nemůžu koupit");
        hra.getHrac().setPenize(30);
        String res3 = hra.zpracujPrikaz("koupit krabicak");
        Assert.assertEquals(res3, "Konečně si můžu koupit krabicák");
    }
}
