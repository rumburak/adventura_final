package logika;
import adventura.logika.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrikazSeberTest {
    private Hra hra;

    @Before
    public void setUp() {
        hra = new Hra();
    }

    @Test
    public void seberTest() {
        hra.zpracujPrikaz("seber vajgl");
        Assert.assertFalse(hra.getHerniPlan().getAktualniProstor().vecExistuje("vajgl"));
        Assert.assertTrue(hra.getHrac().getBatoh().jeVBatohu("vajgl"));
    }
}
