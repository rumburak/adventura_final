package logika;
import adventura.logika.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrikazDejTest {

    private Hra hra;

    @Before
    public void setUp() {
        hra = new Hra();
    }

    @Test
    public void dejTest() {
        hra.getHrac().getBatoh().vlozDoBatohu(new Vec("matecak", true));
        hra.getHrac().getBatoh().vlozDoBatohu(new Vec("vajgl", true));
        String res = hra.zpracujPrikaz("dej vajgl maugli");
        Assert.assertEquals(res, "tuhle věc postava nechce");

        String res1 = hra.zpracujPrikaz("dej toluen maugli");
        Assert.assertEquals(res1, "tuhle věc nemám v batohu");

        String res2 = hra.zpracujPrikaz("dej matecak maugli");
        Assert.assertFalse(hra.getHrac().getBatoh().jeVBatohu("matecak"));

    }
}
