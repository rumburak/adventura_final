package logika;
import adventura.logika.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrikazZabijTest {

    private Hra hra;

    @Before
    public void setUp() {
        hra = new Hra();
    }

    @Test
    public void zabijTest() {
        String res0 = hra.zpracujPrikaz("zabit maugli");
        Assert.assertEquals(res0, "nemám ho jak zabít");
        hra.getHrac().getBatoh().vlozDoBatohu(new Vec("nuz", true));
        String res = hra.zpracujPrikaz("zabit maugli");
        Assert.assertEquals(res, "zabil si mauglího matečáka");

        String res1 = hra.zpracujPrikaz("zabit rumburak");
        Assert.assertEquals(res1, "tuhle postavu nebudu zabíjet");

        String res2 = hra.zpracujPrikaz("zabit bubblegum");
        Assert.assertEquals(res2, "tato postava tady není");
    }



}
