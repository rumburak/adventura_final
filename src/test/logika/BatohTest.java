package logika;
import adventura.logika.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BatohTest {

    /**
     * testovaci trida pro tridu Batoh
     */

    public Batoh batoh;
    public Vec vec1;
    public Vec vec2;
    public Vec vec3;
    @Before
    public void setUp() {
        batoh = new Batoh(2);
        vec1 = new Vec("kamera", true);
        vec2 = new Vec("fotak", true);
        vec3 = new Vec("tuzka", true);
    }
    @Test
    public void testVlozDoBatohu() {
        Assert.assertTrue(batoh.vlozDoBatohu(vec1));
        batoh.vlozDoBatohu(vec2);
        Assert.assertFalse(batoh.vlozDoBatohu(vec3));
    }

    @Test
    public void testOdstranZbatohu() {
        batoh.vlozDoBatohu(vec1);
        Assert.assertTrue(batoh.odstranZBatohu("kamera"));
        Assert.assertFalse(batoh.odstranZBatohu("milion dolaru"));
    }
    @Test
    public void vypisSeznam() {
        String veci = vec2.getNazev() + " " + vec1.getNazev();
        batoh.vlozDoBatohu(vec1);
        batoh.vlozDoBatohu(vec2);
        Assert.assertEquals(veci, batoh.vypisSeznamVeci());
    }
}
