package logika;
import adventura.logika.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TajnePrikaziTest {
    private Hra hra;

    @Before
    public void setUp() {
        hra = new Hra();
    }

    @Test
    public void testSlepice() {
        hra.zpracujPrikaz("jdi popelnice");
        hra.zpracujPrikaz("jdi tesco");
        hra.zpracujPrikaz("slepice krabicak");
        Assert.assertTrue(hra.konecHry());
    }
}
